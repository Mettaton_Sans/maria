program project1;
uses crt,dos;
const kol=6;{������⢮ �㭪⮢ ����}
      {���ᨢ �������� �㭪⮢ ����}
      vybor:array[1..kol] of string = ('���������� ������ �ᯨᠭ�� �४�饭�� �������� ',
                                       '�������� �ᯨᠭ�� �४�饭�� ��������          ',
                                       '�뢮� ���� ������                                 ',
                                       '����� ���� ������ �� ⥪⮢��� 䠩��            ',
                                       '������ ���� ������ � ⥪�⮢� 䠩�               ',
                                       '��室                                             ');
      number=74; {������⢮ �⠭権}
      shedNumber=20; {������⢮ �४�饭�� �������� � �ᯨᠭ��}
type
{������ �⠭��}
	station= Record
		key: integer; {���� �⠭樨}
		name: string[26]; {�������� �⠭樨}
                line:string[20];{�������� �����}
		GPS1: string[5]; {���न���� ���� �⠭樨}
                GPS2: string[5]; {���न���� ������� �⠭樨}
		pass_flow: string[6]; {��⮪ ���ᠦ�஢}
	end;
{������ ⠡��� �४�饭�� ��������}
        shedule= Record
        	key: integer; {���� �४�饭�� ��������}
        	startDate: string[16]; {�६� ��砫� �४�饭�� ��������} { �ਬ�� : 01.01.16 23:45}
        	endDate: string[16]; {�६� ����砭�� �४�饭�� ��������} { �ਬ�� : 01.01.16 23:45}
        	startStationKey: integer; {���� ��砫쭮� �⠭樨}
                endStationKey: integer; {���� ����筮� �⠭樨}
        end;
{��楤�� �⥭�� ������ �� 䠩�� csv}
procedure readtext(var stations : Array of station);
var f1,f2:text;
    i,j,k:integer;
    s: Array[1..80] of string [100];
    temp:string[2];
    temp1:string[1];
    temp_name:string[40];
begin
i:=1;
k:=1;
j:=1;
assign (f1,'stations.csv');
reset(f1);
while not eof(f1) do
 begin
  readln(f1,s[i]);
  inc(i);
 end;
close(f1);

 for i:=1 to number do begin
 {�⥭�� ID �⠭樨 � ��������}
 temp:='  ';
 temp1:=' ';
 if(s[i,2] <> ',') then begin
   temp[1]:=s[i,1];
   temp[2]:=s[i,2];
   Val(temp,stations[i-1].key);
 end
 else begin
  temp1[1]:=s[i,1];
  Val(temp1,stations[i-1].key);
 end;

 {�⥭�� �������� �⠭樨 � ��������}
 temp_name:='                                      ';
 k:=pos(',',s[i]);
 j:=1;
 inc(k);
 while s[i,k] <> ',' do begin
  temp_name[j]:=s[i,k];
  inc(j);
  inc(k);
  end;
  stations[i-1].name:=temp_name;

  {�⥭�� �������� ����� �⠭樨 � ��������}
  temp_name:='                                      ';
  j:=1;
  inc(k);
  while s[i,k] <> ',' do begin
   temp_name[j]:=s[i,k];
   inc(j);
   inc(k);
  end;
  stations[i-1].line:=temp_name;

  {�⥭�� ���न��� (����) � ��������}
  temp_name:='                                      ';
  j:=1;
  inc(k);
  while s[i,k] <> ',' do begin
   temp_name[j]:=s[i,k];
   inc(j);
   inc(k);
  end;
  stations[i-1].GPS1:=temp_name;

  {�⥭�� ���न��� (�������) � ��������}
  temp_name:='                                      ';
  j:=1;
  inc(k);
  while s[i,k] <> ',' do begin
   temp_name[j]:=s[i,k];
   inc(j);
   inc(k);
  end;
  stations[i-1].GPS2:=temp_name;

  {�⥭�� ��⮪� ���ᠦ�஢ � ��⪨ � ��������}
  temp_name:='                                      ';
  j:=1;
  inc(k);
  while j<6 do begin
   temp_name[j]:=s[i,k];
   inc(j);
   inc(k);
  end;
  stations[i-1].pass_flow:=temp_name;

 end;
end;

{��楤�� �⥭�� �ᯨᠭ�� �४�饭�� �������� �� 䠩��}
procedure read_shedule(var shedules : Array of shedule);
var f1:text;
    i,j,k:integer;
    s: Array[1..100] of string [100];
    temp:string[2];
    temp1:string[1];
    temp_name:string[20];
begin
i:=1;
k:=1;
j:=1;
assign (f1,'shedule.csv');
reset(f1);
while not eof(f1) do
 begin
  readln(f1,s[i]);
  s[i]:=s[i]+';';
  //writeln(s[i]);
  inc(i);
 end;
close(f1);

for i:=1 to shedNumber do begin
{�⥭�� ID �४�饭�� �������� � ��������}
 temp:='  ';
 temp1:='  ';
 if(s[i,2] <> ',') then begin
   temp[1]:=s[i,1];
   temp[2]:=s[i,2];
   Val(temp,shedules[i-1].key);
 end
 else begin
  temp1[1]:=s[i,1];
  Val(temp1,shedules[i-1].key);
 end;

 if(shedules[i-1].key < 1) then break; // �� ���뢠�� ���⮩ �������
 {�⥭�� �६��� ��砫� �४�饭�� �������� � ��������}
 temp_name:='                                      ';
 k:=pos(',',s[i]);
 j:=1;
 inc(k);
 while s[i,k] <> ',' do begin
  temp_name[j]:=s[i,k];
  inc(j);
  inc(k);
  end;
  shedules[i-1].startDate:=temp_name;
  {�⥭�� �६��� ���� �४�饭�� �������� � ��������}
 temp_name:='                                      ';
 j:=1;
 inc(k);
 while s[i,k] <> ',' do begin
  temp_name[j]:=s[i,k];
  inc(j);
  inc(k);
  end;
  shedules[i-1].endDate:=temp_name;

  {�⥭�� ID ��砫쭮� �⠭樨 � ��������}
 temp:='  ';
 temp1:='  ';
 inc(k);
 if(s[i,k+1] <> ',') then begin
   temp[1]:=s[i,k];
   temp[2]:=s[i,k+1];
   Val(temp,shedules[i-1].startStationKey);
   inc(k,2);
 end
 else begin
  temp1[1]:=s[i,k];
  Val(temp1,shedules[i-1].startStationKey);
  inc(k);
 end;

 {�⥭�� ID ����筮� �⠭樨 � ��������}
  temp:='  ';
  temp1:='  ';
  inc(k);
  if(s[i,k+1] <> ';') then begin
    temp[1]:=s[i,k];
    temp[2]:=s[i,k+1];
    Val(temp,shedules[i-1].endStationKey);
    inc(k,2);
  end
  else begin
   temp1[1]:=s[i,k];
   Val(temp1,shedules[i-1].endStationKey);
   inc(k);
  end;
 end;
writeln('�����襭�');
end;



{
procedure write_to_file(var students : Array of student);
var i,k:integer;
    {⥪�⮢� ���� � ����⠬� ��㤥��}
var studentfile: text;

begin
 k:=1;
 assign(studentfile,'stud_file.csv');
 rewrite(studentfile);
 for i:=0 to number-1 do begin
  write(studentfile,students[i].key);
  write(studentfile,',');
  write(studentfile,students[i].surname);
  write(studentfile,',');
  write(studentfile,students[i].name);
  write(studentfile,',');
  write(studentfile,students[i].last_name);
  write(studentfile,',');
  write(studentfile,students[i].pol);
  write(studentfile,',');
  writeln(studentfile,students[i].age);
 end;
 close(studentfile);
end;
   }


procedure create_db(var stations : Array of station; var shedules : Array of shedule);

var i:integer;
    keymax:integer;
begin
    keymax:=0;
    for i:=1 to shedNumber do begin
      if(shedules[i].key > keymax) then keymax:= shedules[i].key;
    end;
    shedules[keymax+1].key := keymax+1;
    writeln('Key ������ �ᯨᠭ�� = ', keymax+1);
    write('������ ���� � �६� ��砫� �४�饭�� ��������');
    readln(shedules[keymax+1].startDate);
    write('������ ���� � �६� ����砭�� �४�饭�� ��������');
    readln(shedules[keymax+1].endDate);
    write('������ ���� ��砫쭮� �⠭樨');
    readln(shedules[keymax+1].startStationKey);
    write('������ ���� ����筮� �⠭樨');
    readln(shedules[keymax+1].endStationKey);

end;

procedure delete_shedule(var stations : Array of station; var shedules : Array of shedule);
var i,j:integer;
    keymax:integer;
    delete_number:integer;
begin
  write('������ ����� �ᯨᠭ��: ');
  readln(delete_number);
  for i:=1 to shedNumber do begin
     if(shedules[i].key = delete_number) then delete_number := i;
  end;
  for j:=delete_number to shedNumber do begin
   shedules[j]:=shedules[j+1];
  end;
  shedules[shedNumber].key:=0;
  writeln('�������� �����襭� ');
  readln;
end;

procedure Menu(var k:byte);{ᮧ����� � �뢮� �� ��࠭ ����}
var kod: char;
    i:byte;
begin
textbackground(0);
clrscr;
k:=1;
gotoxy(4,1);
write('����');
k:=1; {�뢥��� ���� �㭪� ����}
repeat
for i:=1 to kol do
 begin
  if i=k then {�뤥����� �㭪�}
   begin
    textbackground(15);
    textcolor(0);
   end
  else  {��⠫��}
   begin
    textbackground(0);
    textcolor(15)
   end;
gotoxy(1,i+1);{�⠢�� �����}
write(vybor[i]);{�뢮��� �㭪��}
end;
repeat
kod:=readkey;
until Kod in [#13, #72, #80];
case Kod of
#72: begin{��५�� �����}
     k:=k-1;
     if k=0 then k:=kol;{�᫨ ��� ����, ����}
     end;
#80: begin {��५�� ����}
     k:=k+1;
     if k=kol+1 then k:=1;{�᫨ ���� ����, �����}
     end;
end;
until kod=#13;{����� Enter, ��室�� �� ���� � ��࠭��� ��楤���}
end;


{�᭮���� �ணࠬ��}
var k:byte;
    i:integer;
    stations : Array[1..80] of station; {���ᨢ ������� ⨯� station}
    shedules : Array[1..80] of shedule; {���ᨢ ������� ⨯� shedules}
begin
 repeat
   textbackground(0);
   textcolor(15);
   Menu(k);{�뢮��� ����}
   clrscr;
   case k of{�롨ࠥ� ��५���� ����⢨�}
   1: begin
       writeln('����� ��楤��� ���������� ������� ���� ������');
       create_db(stations,shedules);
        readln;
       end;
   2: begin
      writeln('����� ��楤��� 㤠����� ������� �� ���� ������');
      delete_shedule(stations,shedules);
      writeln('�������� �믮�����');
      end;
   3: begin
       writeln('�뢮� ���� ������');
       writeln(' _______________________________________________________________________ ');
       writeln('|key |', '�⠭��                   |', '�����               |', 'GPS1 |GPS2 |', '��⮪ |  ');
       for i:=1 to number do begin
       if(stations[i].key > 0) then begin
        writeln('|____|__________________________|____________________|_____|_____|______|');
        if(stations[i].key < 10) then begin
          write('|  ');
          write(stations[i].key,' |')
        end
        else begin
          write('| ');
          write(stations[i].key,' |');
        end;
        if((stations[i].key >=1) and (stations[i].key <=23)) then textcolor(12);
        if((stations[i].key >=24) and (stations[i].key <=44)) then textcolor(9);
        if((stations[i].key >=45) and (stations[i].key <=54)) then textcolor(10);
        if((stations[i].key >=55) and (stations[i].key <=62)) then textcolor(14);
        if((stations[i].key >=63) and (stations[i].key <=74)) then textcolor(13);
        write(stations[i].name);
        textcolor(15);
        write('|');
        if((stations[i].key >=1) and (stations[i].key <=23)) then textcolor(12);
        if((stations[i].key >=24) and (stations[i].key <=44)) then textcolor(9);
        if((stations[i].key >=45) and (stations[i].key <=54)) then textcolor(10);
        if((stations[i].key >=55) and (stations[i].key <=62)) then textcolor(14);
        if((stations[i].key >=63) and (stations[i].key <=74)) then textcolor(13);
        write(stations[i].line);
        textcolor(15);
        write('|');
        write(stations[i].GPS1,'|',stations[i].GPS2,'|');
        writeln(stations[i].pass_flow, '|');
      end;
       end;
      writeln('|____|__________________________|____________________|_____|_____|______|');
      readln;
      writeln('�뢮� �४�饭�� ��������');
      writeln(' _______________________________________________________________________ ');
      writeln('|key |', '  ��砫� / ����砭��   |', '    �� �⠭樨 /  �� �⠭樨�             |');
      for i:=1 to shedNumber do begin
       if(shedules[i].key > 0) then begin
        writeln('|____|_______________________|__________________________________________|');
        write('|    | ');
        write(shedules[i].startDate);
        write('/     |  ');
        if((shedules[i].startStationKey >=1) and (shedules[i].startStationKey <=23)) then textcolor(12);
        if((shedules[i].startStationKey >=24) and (shedules[i].startStationKey <=44)) then textcolor(9);
        if((shedules[i].startStationKey >=45) and (shedules[i].startStationKey <=54)) then textcolor(10);
        if((shedules[i].startStationKey >=55) and (shedules[i].startStationKey <=62)) then textcolor(14);
        if((shedules[i].startStationKey >=63) and (shedules[i].startStationKey <=74)) then textcolor(13);
        write(stations[shedules[i].startStationKey].name);
        textcolor(15);
        writeln('/             |');

        if(shedules[i].key < 10) then begin
          write('|  ');
          write(shedules[i].key,' | ')
        end
        else begin
          write('| ');
          write(shedules[i].key,' | ');
        end;
        write(shedules[i].endDate);
        write('      |  ');
        if((shedules[i].endStationKey >=1) and (shedules[i].endStationKey <=23)) then textcolor(12);
        if((shedules[i].endStationKey >=24) and (shedules[i].endStationKey <=44)) then textcolor(9);
        if((shedules[i].endStationKey >=45) and (shedules[i].endStationKey <=54)) then textcolor(10);
        if((shedules[i].endStationKey >=55) and (shedules[i].endStationKey <=62)) then textcolor(14);
        if((shedules[i].endStationKey >=63) and (shedules[i].endStationKey <=74)) then textcolor(13);
        write(stations[shedules[i].endStationKey].name);
        textcolor(15);
        writeln('              |');
       end;
       end;
      writeln('|____|_______________________|__________________________________________|');
      readln;
       end;
   4: begin
       writeln('���뢠��� ���� ������ �� ⥪�⮢��� 䠩��');
       readtext(stations);
       read_shedule(shedules);
       readln;
      end;
   5: begin
       writeln('������ � 䠩� stud_file.csv');
       //write_to_file(students);
       readln;
      end;
   6: exit;
  end;
 until k=5;
end.


